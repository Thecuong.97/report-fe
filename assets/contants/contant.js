
// Different method for today

// Export Constant category

// Export Constant gender
export const GENDER = [
  {
    id: 0,
    name: 'Male'
  },
  {
    id: 1,
    name: 'Female'
  }
]

// Export Constant role
export const ROLE = [
  {
    id: 0,
    name: 'Menber'
  },
  {
    id: 1,
    name: 'Management'
  }
]

export const AVATAR_MALE = 'https://1.bp.blogspot.com/-A7UYXuVWb_Q/XncdHaYbcOI/AAAAAAAAZhM/hYOevjRkrJEZhcXPnfP42nL3ZMu4PvIhgCLcBGAsYHQ/s1600/Trend-Avatar-Facebook%2B%25281%2529.jpg';
export const AVATAR_FEMALE = 'https://1.bp.blogspot.com/-TU1Uqzc3FWY/XncdLdnBvKI/AAAAAAAAZh8/0Cw7NXbO0q4v_zlYSjEnZIV_xHfnWiwxQCLcBGAsYHQ/s1600/Trend-Avatar-Facebook%2B%25282%2529.jpg';

// Export Constant url API
export const BASE_API = "http://127.0.0.1:8000/api/"
export const API_ALL = BASE_API + "all/"
export const API_COMMENT = BASE_API + "comments/"
export const API_USER = BASE_API + 'users/'
export const API_TEAM = BASE_API + 'teams/'
export const API_REPORT = BASE_API + 'reports/'
export const API_AUTH_USER = BASE_API + 'auth/user'
export const API_LIST_USER_BY_TEAM = BASE_API + 'teams/users/'
export const API_GET_TEAM_BY_USER = BASE_API + 'users/team/'
